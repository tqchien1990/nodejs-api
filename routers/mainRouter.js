const config = require('../common/config/env.config');
const accountRouter =require('../routers/accountRouter');
const elasticSearchRouter =require('../routers/elasticSearchRouter');

exports.routesConfig = function (app) {
    accountRouter.routesConfig(app)
    elasticSearchRouter.routesConfig(app)
}


