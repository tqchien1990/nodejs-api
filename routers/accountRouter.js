const AccountController = require('../controllers/accountController');
const PermissionMiddleware = require('../common/middlewares/auth.permission.middleware');
const ValidationMiddleware = require('../common/middlewares/auth.validation.middleware');
const config = require('../common/config/env.config');

const ADMIN = config.permissionLevels.ADMIN;
const USER = config.permissionLevels.USER;
const GUEST = config.permissionLevels.GUEST;

exports.routesConfig = function (app) {
    app.get('/account', [
        ValidationMiddleware.validJWTNeeded,
        PermissionMiddleware.minimumPermissionLevelRequired(ADMIN),
        AccountController.list
    ]);

    app.get('/getView', [
        ValidationMiddleware.validJWTNeeded,
        PermissionMiddleware.minimumPermissionLevelRequired(ADMIN),
        AccountController.getView
    ]);

    app.post('/createAccount', [
        ValidationMiddleware.validJWTNeeded,
        PermissionMiddleware.minimumPermissionLevelRequired(ADMIN),
        AccountController.createAccount
    ]);

    app.post('/updateAccount', [
        ValidationMiddleware.validJWTNeeded,
        PermissionMiddleware.minimumPermissionLevelRequired(ADMIN),
        AccountController.updateAccountById
    ]);
};