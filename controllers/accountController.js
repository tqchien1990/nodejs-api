const responseClass = require('../common/config/responseClass');
const accountModel = require('../models/account');
const bcrypt = require('bcrypt-nodejs');

exports.createAccount = (req, res) => {
    req.body.password = bcrypt.hashSync(req.body.password);
    //req.body.permissionLevel = 1;
    accountModel.createAccount(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        })
        .catch(function (error) {
            res.status(400).send(error);
        });
};

exports.list = (req, res) => {
    let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
    let page = 0;
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }
    accountModel.list(limit, page)
        .then((result) => {
            let resp = new responseClass();
            resp.count = Object.keys(result).length;
            resp.data = result
            res.status(200).send(resp);
        })
};

exports.getById = (req, res) => {
    accountModel.findById(req.params.userId)
        .then((result) => {
            res.status(200).send(result);
        });
};

exports.updateAccountById = (req, res) => {
    accountModel.updateAccountById(req.body.id, req.body)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch(function (error) {
            res.status(400).send(error);
        });
};

exports.removeById = (req, res) => {
    accountModel.removeById(req.params.userId)
        .then((result) => {
            res.status(204).send({});
        });
};

exports.getView = (req, res) => {
    accountModel.getView()
        .then((result) => {
            res.status(200).send(result);
        });
};