var elasticSearch = require('elasticsearch');
require('dotenv').config();

var conn = process.env.ELASTIC_URI || "localhost"

var client = new elasticSearch.Client({
    hosts: [
        conn
    ]
});
module.exports = client;