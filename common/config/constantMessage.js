module.exports = {
    "ERROR_MESSAGE": {
        "400": "Tham số không hợp lệ.",
        "401": "Lỗi xác thực.",
        "403": "Hạn chế quyền truy cập.",
        "404": "Không tìm thấy.",
        "200": "OK",
        "500": "Lỗi từ máy chủ."
    }
};
