const mongoose = require('mongoose');
require('dotenv').config();

var conn = process.env.MONGO_URI || "localhost"

const db = mongoose.connect(conn, {useNewUrlParser: true}).then(
    () => {
        console.log("Connected!");
    },
    err => {
        throw err
    }
);
module.exports = mongoose