require('dotenv').config();

module.exports = {
    "port": process.env.APP_PORT||3600,
    "appEndpoint": "http://localhost:3600",
    "apiEndpoint": "http://localhost:3600",
    "jwt_secret": "mango",
    "jwt_expiration_in_seconds": 36000,
    "environment": "dev",
    "permissionLevels": {
        "GUEST": 1,
        "USER": 2,
        "ADMIN": 3
    }
};
