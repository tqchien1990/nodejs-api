const responseClass = require('../common/config/responseClass');
const client = require('../common/config/elasticSearch');
var moment = require('moment');

require('dotenv').config();

exports.searchPost = (input) => {
    return new Promise((resolve, reject) => {
        let queryString = {};
        let aggString = {};
        queryString.bool = {};
        let mustBuilder = queryString.bool.must = [];
        let mustNotBuilder = queryString.bool.must_not = [];
        let aggsBuilder = aggString.total = {};
        if (!input.projectId) {
            if (input.keywords) {
                mustBuilder.push({prefix: {"keywords.keyword": input.keywords}});
            }
            if (input.dateFrom && input.dateTo) {
                mustBuilder.push({
                    range: {
                        createDate: {
                            from: input.dateFrom,
                            to: input.dateTo,
                            format: "dd/MM/yyyy",
                            time_zone: "+0700"
                        }
                    }
                });
            }
            if (input.type) {
                mustBuilder.push({terms: {"type.keyword": input.type}});
            }
            if (input.labels) {
                mustNotBuilder.push({term: {"labels.Sell Detection": input.labels}});
            }

            if (mustBuilder.length === undefined || mustBuilder.length == 0) {
                queryString.bool.must = {match_all: {}};
            }
        }
        //aggs
        aggsBuilder.terms = {field: "keywords.keyword", size: 10}

        //sort
        let sortOption = [];
        if (input.sort === "share") {
            sortOption = [{numShares: {order: "desc"}}];
        }
        else if (input.sort === "like") {
            sortOption = [{numLikes: {order: "desc"}}];
        }
        else if (input.sort === "time") {
            sortOption = [{createDate: {order: "desc"}}];
        }
        let clientSearch = client.search({
            index: process.env.ELASTIC_INDEX_POST || "ospg_rad_post_*",
            type: 'post',
            size: input.size,
            from: input.from,
            body: {
                query: queryString,
                aggs: aggString,
                sort: sortOption
            },
        }, function (error, response, status) {
            if (error) {
                reject(error);
            }
            else {
                var lst = [];
                response.hits.hits.forEach(function (hit) {
                    lst.push({
                        "userId": hit._source.userId,
                        "name": hit._source.name
                    });

                })

                let resp = new responseClass();
                resp.count = response.hits.total;
                resp.data = response.hits.hits;
                resp.data = response;
                resolve(resp)
            }
        });
        /*console.log(JSON.stringify(sortOption))
        console.log(JSON.stringify({query: queryString}));*/
    });
};

exports.searchPage = (input) => {
    return new Promise((resolve, reject) => {
        let queryString = {};
        let aggString = {};
        queryString.bool = {};
        let mustBuilder = queryString.bool.must = [];
        let mustNotBuilder = queryString.bool.must_not = [];
        let shouldBuilder = queryString.bool.should = [];
        let aggsBuilder = aggString.total = {};

        //
        if (!input.projectId) {
            mustBuilder.push({exists: {field: "pageid.keyword"}})
            if (input.keywords) {
                mustBuilder.push({
                    bool: {
                        should: [
                            {
                                multi_match: {
                                    query: input.keywords,
                                    fields: ["content", "message"],
                                    type: "phrase",
                                    slop: 2
                                }
                            }
                        ]
                    }
                });
            }
            if (input.dateFrom && input.dateTo) {
                mustBuilder.push({
                    range: {
                        createDate: {
                            from: input.dateFrom,
                            to: input.dateTo,
                            format: "dd/MM/yyyy",
                            time_zone: "+0700"
                        }
                    }
                });
            }
            if (input.type) {
                mustBuilder.push({terms: {"type.keyword": input.type}});
            }
            if (input.labels) {
                mustNotBuilder.push({term: {"labels.Sell Detection": input.labels}});
            }

            if (mustBuilder.length === undefined || mustBuilder.length == 0) {
                queryString.bool.must = {match_all: {}};
            }
        }
        aggsBuilder.terms = {field: "pageid.keyword", size: 1000}
        //
        var getDaysArray = function (start, end) {
            for (var arr = [], dt = moment(start, "DD-MM-YYYY").toDate(); dt <= moment(end, "DD-MM-YYYY").toDate(); dt.setDate(dt.getDate() + 1)) {
                arr.push(new Date(dt));
            }
            return arr;
        };
        var listIndex = getDaysArray(input.dateFrom, input.dateTo);
        listIndex = listIndex.map((v) => process.env.ELASTIC_INDEX_POST + v.toISOString().slice(0, 10)).join(",")

        let clientSearch = client.search({
            //index: process.env.ELASTIC_INDEX_POST || "ospg_rad_post_*",
            index: listIndex || "ospg_rad_post_*",
            type: 'post',
            // size: input.size,
            // from: input.from,
            body: {
                query: queryString,
                aggs: aggString
            },
        }, function (error, response, status) {
            if (error) {
                reject(error);
            }
            else {
                let listUserId = [];
                console.log(response.hits.hits);
                response.aggregations.total.buckets.forEach(function (bucket) {
                    listUserId.push({"pageId": bucket.key});
                })

                if (listUserId) {
                    let queryString = {};
                    queryString.bool = {};
                    let mustBuilder = queryString.bool.must = [];
                    let shouldBuilder = queryString.bool.should = [];
                    shouldBuilder.push({terms: {"pageId.keyword": listUserId}});
                    client.search({
                        index: process.env.ELASTIC_INDEX_PAGE || "ospg_page",
                        type: 'page',
                        size: input.size,
                        from: input.from,
                        body: {
                            query: queryString
                        },
                    }, function (error, response, status) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            let resp = new responseClass();
                            // resp.count = response.hits.total;
                            // resp.data = response.hits.hits;
                            resp.data = response;
                            resolve(resp)
                        }
                    });
                }
            }
        });
    });
};

exports.searchUser = (input) => {
    return new Promise((resolve, reject) => {
        let queryString = {};
        let aggString = {};
        queryString.bool = {};
        let mustBuilder = queryString.bool.must = [];
        let mustNotBuilder = queryString.bool.must_not = [];
        let shouldBuilder = queryString.bool.should = [];
        let aggsBuilder = aggString.total = {};

        if (!input.projectId) {
            if (input.keywords) {
                mustBuilder.push({
                    bool: {
                        should: [
                            {
                                multi_match: {
                                    query: input.keywords,
                                    fields: ["content", "message"],
                                    type: "phrase",
                                    slop: 2
                                }
                            }
                        ]
                    }
                });
            }
            if (input.dateFrom && input.dateTo) {
                mustBuilder.push({
                    range: {
                        createDate: {
                            from: input.dateFrom,
                            to: input.dateTo,
                            format: "dd/MM/yyyy",
                            time_zone: "+0700"
                        }
                    }
                });
            }
            if (input.type) {
                mustBuilder.push({terms: {"type.keyword": input.type}});
            }
            if (input.labels) {
                mustNotBuilder.push({term: {"labels.Sell Detection": input.labels}});
            }

            if (mustBuilder.length === undefined || mustBuilder.length == 0) {
                queryString.bool.must = {match_all: {}};
            }
        }
        aggsBuilder.terms = {field: "userid.keyword", size: 1000}
        //
        var getDaysArray = function (start, end) {
            for (var arr = [], dt = moment(start, "DD-MM-YYYY").toDate(); dt <= moment(end, "DD-MM-YYYY").toDate(); dt.setDate(dt.getDate() + 1)) {
                arr.push(new Date(dt));
            }
            return arr;
        };
        var listIndex = getDaysArray(input.dateFrom, input.dateTo);
        listIndex = listIndex.map((v) => process.env.ELASTIC_INDEX_POST + v.toISOString().slice(0, 10)).join(",")

        let clientSearch = client.search({
            //index: process.env.ELASTIC_INDEX_POST || "ospg_rad_post_*",
            index: listIndex || "ospg_rad_post_*",
            type: 'post',
            // size: input.size,
            // from: input.from,
            body: {
                query: queryString,
                aggs: aggString
            },
        }, function (error, response, status) {
            if (error) {
                reject(error);
            }
            else {
                let listUserId = [];
                console.log(response.hits.hits);
                response.aggregations.total.buckets.forEach(function (bucket) {
                    listUserId.push({"userid": bucket.key});
                })

                if (listUserId) {
                    let queryString = {};
                    queryString.bool = {};
                    let mustBuilder = queryString.bool.must = [];
                    let shouldBuilder = queryString.bool.should = [];
                    shouldBuilder.push({terms: {"userId.keyword": listUserId}});
                    client.search({
                        index: process.env.ELASTIC_INDEX_USER || "ospg_user",
                        type: 'user',
                        size: input.size,
                        from: input.from,
                        body: {
                            query: queryString
                        },
                    }, function (error, response, status) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            let resp = new responseClass();
                            // resp.count = response.hits.total;
                            // resp.data = response.hits.hits;
                            resp.data = response;
                            resolve(resp)
                        }
                    });
                }
            }
        });
    });
};