// const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost:27017/aim', { useNewUrlParser: true })
const dbCon = require('../common/config/mongoDb');
const Schema = dbCon.Schema;

const accountSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    username: String,
    permissionLevel: Number,
    createDate: Date,
    updateDate: Date
}, {
    collection: 'account'
});

const accountViewSchema = new Schema({
    _id: String,
    ngayFormat: String
});

accountSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
accountSchema.set('toJSON', {
    virtuals: true
});

accountSchema.findById = function (cb) {
    return this.model('Account').find({id: this.id}, cb);
};

const Account = dbCon.model('account', accountSchema);
const accountViewModel = dbCon.model('account_view', accountViewSchema, 'account_view');


exports.findByEmail = (email) => {
    return Account.find({email: email});
};

exports.findByUsername = (username) => {
    return Account.find({username: username});
};

exports.findById = (id) => {
    return Account.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createAccount = (accountData) => {
    return new Promise((resolve, reject) => {
        const account = new Account(accountData);
        account.createDate = new Date();
        account.save(function (err, createdAccount) {
            if (err) {
                reject(err);
            }
            else {
                createdAccount = createdAccount.toJSON();
                delete createdAccount.password;
                resolve(createdAccount);
            }
        });


    });

};

exports.getView = () => {
    return new Promise((resolve, reject) => {
        accountView111.find().exec(function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Account.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, users) {
                if (err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            })
    });
};

exports.updateAccountById = (id, accountData) => {
    return new Promise((resolve, reject) => {
        Account.findById(id, function (err, account) {
            if (err || !account) {
                reject({message: "Không tìm thấy tài khoản."});
            }
            else {
                for (let i in accountData) {
                    if (i.equals == 'userName' || i == 'password') {
                        continue;
                    }
                    account[i] = accountData[i];
                }
                account.updateDate = new Date();
                account.save(function (err, updatedAccount) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        updatedAccount = updatedAccount.toJSON();
                        delete updatedAccount.password;
                        resolve(updatedAccount);
                    }
                });
            }
        });
    })

};

exports.removeById = (userId) => {
    return new Promise((resolve, reject) => {
        User.remove({_id: userId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

