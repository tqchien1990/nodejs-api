const UserModel = require('../../models/account');
const crypto = require('crypto');
const bcrypt = require('bcrypt-nodejs');

exports.hasAuthValidFields = (req, res, next) => {
    let errors = [];

    if (req.body) {
        if (!req.body.userName) {
            errors.push('Thiếu thông tin tài khoản');
        }
        if (!req.body.password) {
            errors.push('Thiếu thông tin mật khẩu');
        }

        if (errors.length) {
            return res.status(400).send({errors: errors.join(' ,')});
        } else {
            return next();
        }
    } else {
        return res.status(400).send({errors: 'Thiếu thông tin tài khoản và mật khẩu.'});
    }
};
exports.isPasswordAndUserMatch = (req, res, next) => {
    UserModel.findByUsername(req.body.userName).then(user => {
        var _req = req;
        var _res = res;
        if (!user[0]) {
            res.status(404).send({errors: "Sai tài khoản hoặc mật khẩu."});
        } else {
            let passwordInput = req.body.password;
            let passwordFields = user[0].password;
            bcrypt.compare(passwordInput, passwordFields, function (err, res) {
                if (res) {
                    _req.body = {
                        userId: user[0]._id,
                        userName: user[0].username,
                        email: user[0].email,
                        // permissionLevel: user[0].permissionLevel,
                        permissionLevel: 7,
                        displayName: user[0].firstName + ' ' + user[0].lastName,
                    };
                    return next();
                }
                else {
                    return _res.status(400).send({errors: err ? err : 'Sai tài khoản hoặc mật khẩu.'});
                }
            });
        }
    })
};
exports.isPasswordAndUserMatch1 = (req, res, next) => {
    UserModel.findByEmail(req.body.email)
        .then((user) => {
            if (!user[0]) {
                res.status(404).send({});
            } else {
                let passwordFields = user[0].password.split('$');
                let salt = passwordFields[0];
                let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
                if (hash === passwordFields[1]) {
                    req.body = {
                        userId: user[0]._id,
                        email: user[0].email,
                        // permissionLevel: user[0].permissionLevel,
                        permissionLevel: 7,
                        provider: 'email',
                        name: user[0].firstName + ' ' + user[0].lastName,
                    };
                    return next();
                } else {
                    return res.status(400).send({errors: ['Invalid e-mail or password']});
                }
            }
        });
};